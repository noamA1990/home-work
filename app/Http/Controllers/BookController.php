<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$books = Book::all();
        return view('books.index',['books'=>$books]);*/
        $id = Auth::id();
        if(!Gate::allows('main_user')){
            $main = DB::table('role_users')->where('secondary_user',$id)->first();
            $id = $main->main_user;
        }
        // $id = Auth::id();
        // $books = User::find($id)->books;
        $user = User::find($id);
        $books = $user->books;
        return view('books.index',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
        ]);
        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }
        else {
            echo "<script>";
            echo "alert('New Book added :)');";
            echo "</script>";
            // alert("New Book added :)");
        }
        $book = new Book();
        $id = Auth::id();;
        $book->user_id=$id;
        $book->title=$request->title;
        $book->author=$request->author;
        $book->status=0;
        
        $book ->save();

        return redirect('books')->with('alert','New Book was added :)');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book=Book::find($id);
       
        return view('books.edit', ['books'=>$book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book=Book::findOrFail($id);

        if (!$book->user->id == Auth::id()){
                return (readdirect('books'));
            }

        if (!$request->ajax())
        {
            $v = Validator::make($request->all(), [
                'title' => 'required|max:255',
                'author' => 'required',
            ]);
               
            if ($v->fails())
            {
                return redirect()->back()->withErrors($v->errors());
            }
            $book->update($request->except(['_token']));
        } 
            else 
            {
                $book->update($request->except(['_token']));
                return Response::json(array('result'=>'success', 'status'=>$request->status),200);
            }
            
            return redirect('books')->with('alert','Book was updated :)');

        // if ($request->ajax()){
        //     return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        // }

        // $todo=Todo::find($id);
        // $todo->update($request->all());
        
        // // $book->update($request->all());
        // return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book=Book::find($id);
        if (!$book->user->id == Auth::id()){
            return (readdirect('books'));
        }
        $book->delete();
        return redirect('books')->with('alert','Book was deleted :(');
    }
}
