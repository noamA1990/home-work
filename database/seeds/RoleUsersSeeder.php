<?php

use Illuminate\Database\Seeder;

class RoleUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('role_users')->insert([
            [
                'secondary_user'=>4,
                'main_user' => 1
            ],
            [
                'secondary_user'=>5,
                'main_user' => 6
            ]
        ]);
    }
}
