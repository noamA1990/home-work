<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Noam',
                'email' => "noam@gmail.com",
                'password' =>Hash::make(12345678),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'secondary_user'
            ],
            [
                'name' => 'Ami',
                'email' => "ami@gmail.com",
                'password' => Hash::make(12345678),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'secondary_user'
                ]
        ]);
    }
}
