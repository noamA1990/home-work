@extends('layouts.app')
@section('content')

    <!-- <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> -->
    <!-- <title>Books</title> -->


<h1>This is your Books Table</h1>

<table class = "table table-dark table-hover">
    <tr>
        <th>
            User Id
        </th>
        <th>
            Book Title
        </th>
        <th>
            Author Name
        </th>
        <th>
            Did you read the book?
        </th>
    </tr>
    
    @foreach($books as $book)
    <tr>
        <td>
            {{$book->user_id}}
            </td>
            <td>
            @can('main_user')
            <a href="{{route('books.edit',$book->id)}}">{{$book->title}}</a>
            @endcan
            @cannot('main_user') 
            {{$book->title}}
            @endcannot
            </td>
            <td>
            {{$book->author}}
            </td>
            <td>
            @if ($book->status)
                <input type = 'checkbox' name = 'status' id ="{{$book->id}}" disabled='disable' checked>
            @else
                <input type = 'checkbox' name = 'status' id ="{{$book->id}}">
            @endif
            </td>
    @endforeach
    </tr>
</table>

<br>
@can('main_user')
<a href="{{route('books.create')}}">Create new Book</a>
@endcan

<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               alert("status updated :)");
               $(this).attr('disabled', true);
               $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked,_token:'{{csrf_token()}}'}),
                   processData: false ,
                   success: function(data){
                        console.log(JSON.stringify(data));
                   },
                   error:function(errorThrown){
                       console.log(errorThrown);
                   }
               });
            });      
       });
       
</script>
@if (session('alert'))
<script>
  var msg = '{{Session::get('alert')}}';
  var exist = '{{Session::has('alert')}}';
  if(exist){
    alert(msg);
  }
</script>
@endif

<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>
<!-- 
<style>
    .table{
        width: auto;
    }
</style> -->
<!-- </html> -->
@endsection