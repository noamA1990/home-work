@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" 
        integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUIyJ" crossorigin="anonymous"> -->
    <title>Books</title>
</head>
<body>

<h1>Edit your Book</h1>

<form action="{{action('BookController@update',$books->id)}}" method = 'post'>
    @csrf
    @method('PATCH')
    <div class = "form-group">
        <label for="title"> The name of the book you would like to change:</label>
        <input type="text" class="form-control" name = 'title' value ="{{$books->title}}">
    </div>
    <div>
        <label for="title"> Name of the author of the book you edited: </label>
        <input type="text" class = "form-control" name = 'author' value = "{{$books->author}}">
    </div>

    <div class = "form-group">
        <input type="submit" class = "form-control" name = "submit" value = "Done!">
    </div>
</form>

<form method = 'post' action="{{action('BookController@destroy',$books->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete Book">  
</div>
</form>

<br>
<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

</body>
</html>
@endsection